# Petits programmes crées pour apprendre le python

## Deux anciennes versions

* /poc_dev/banking.py compatible python2
* /poc_dev/banking3.py compatible python3

## Redécoupage du projet banking  

* front_bank_cli.py
    * Front end de l'appli
* translation.py
    * Gestion de la liste des langues

## TODO 

* Gestion du multilangue
* Passage en base de donnée SQLITE3
    * [pymotw sqlite3](https://pymotw.com/3/sqlite3/index.html#module-sqlite3)
    * [Exemple bank avec sqlite3](https://github.com/pranjali97/Bank-management-cli-based)
* Documentation Docstring - en cours
* password avec getpass 
    * [pymotw getpass](https://pymotw.com/3/getpass/index.html#module-getpass)
* UNITTEST
    * [Blog avec exemple unittest sqlite3](https://www.blog.pythonlibrary.org/2016/07/07/python-3-testing-an-intro-to-unittest/)
