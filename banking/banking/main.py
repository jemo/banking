from models.client import Base, Clients
from controllers.client import new_client, print_clients
from sqlalchemy import orm, create_engine


def main():
    engine = create_engine('sqlite:///banking.db', echo=True)
    Base.metadata.bind = engine
    session = orm.sessionmaker(engine)
    Base.metadata.create_all()

    with session() as session:
        client1 = Clients(first_name='Test', second_name='name')
        client2 = Clients(first_name='Test2', second_name='name2')
        new_client(session, client1)
        new_client(session, client2)
        print_clients(session, Clients)


if __name__ == "__main__":
    main()
