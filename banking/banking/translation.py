#!/usr/bin/python
# -*-coding:Utf-8 -*
# jemo 8 juin 2015 mis a jour pour python 3
# jemo 11 juin 2020 mise en forme projet

"""
    Module de gestion des langues
"""

import csv
import logging

# Gestion des log
logging.basicConfig(filename="log", level=logging.DEBUG, \
                    format="%(asctime)s %(message)s", datefmt="%d/%m/%Y %H:%M:%S")


def select_language(language):
    """
    Fonction poursSelectionner la langue pour l'interface CLI
    """
    logging.info(" >> debut select_language() --> STATUS: SUCCESS")
    # creation des liste de langues
    lang = []
    if language == "french":
        filin = "francais.csv"
    elif language == "english":
        filin = "english.csv"
    else:
        return "error"
    fopen = open(filin)
    reader = csv.reader(fopen)
    for row in reader:
        lang = lang + row
    fopen.close()
    return lang


if __name__ == '__main__':
    lang = select_language("french")
    print("lang[0]", lang[0])
