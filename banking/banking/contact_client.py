#!/usr/bin/python
# -*-coding:Utf-8 -*
# jemo 05 novembre 2020 creation

"""
    Module d'entree en contact client
"""

import logging
from banking.initialisation import check_database, create_database

# Gestion des log
logging.basicConfig(filename="log", level=logging.DEBUG, format="%(asctime)s %(message)s", datefmt="%d/%m/%Y %H:%M:%S")


def init_system():
    """
    Fonction pour initialiser le systeme ou vérifier que
    les bases sont présentent
    """
    print("Init system")
    logging.info(" >> debut init_system() --> STATUS: SUCCESS")
    db_filename = "banking.db"
    print(check_database(db_filename))
    if check_database(db_filename):
        pass
    else:
        create_database(db_filename)
    # os.system("clear")


if __name__ == '__main__':
    init_system()
