import sqlite3

class Client:
    def __init__(self, last_name, first_name, address=None, email=None):
        self.last_name = last_name
        self.first_name = first_name
        self.address = address
        self.email = email

    def save_to_database(self):
        conn = sqlite3.connect('banking_database.db')
        cursor = conn.cursor()

        cursor.execute('''
            INSERT INTO Client (last_name, first_name, address, email) 
            VALUES (?, ?, ?, ?)
        ''', (self.last_name, self.first_name, self.address, self.email))

        conn.commit()
        conn.close()

    @staticmethod
    def get_client_by_last_name(last_name):
        conn = sqlite3.connect('banking_database.db')
        cursor = conn.cursor()

        cursor.execute('SELECT * FROM Client WHERE last_name = ?', (last_name,))
        client_data = cursor.fetchone()

        conn.close()

        if client_data:
            client = Client(*client_data[1:])
            return client
        else:
            return None

    def update_client(self):
        conn = sqlite3.connect('banking_database.db')
        cursor = conn.cursor()

        cursor.execute('''
            UPDATE Client 
            SET first_name=?, address=?, email=? 
            WHERE last_name=?
        ''', (self.first_name, self.address, self.email, self.last_name))

        conn.commit()
        conn.close()

# Exemple d'utilisation
# Création d'un nouveau client
new_client = Client(last_name='Smith', first_name='Alice', address='456 Oak St', email='alice.smith@example.com')
new_client.save_to_database()

# Récupération d'un client par son nom de famille
retrieved_client = Client.get_client_by_last_name('Smith')
if retrieved_client:
    print("Client found:", retrieved_client.last_name, retrieved_client.first_name)
    
    # Modification des informations du client
    retrieved_client.address = '789 Maple St'
    retrieved_client.update_client()
    print("Client information updated.")
else:
    print("Client not found.")
