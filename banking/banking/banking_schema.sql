-- jemo 20200622 creation schema database

-- Table for clients
CREATE TABLE Client (
    client_id INTEGER PRIMARY KEY AUTOINCREMENT,
    last_name TEXT NOT NULL,
    first_name TEXT NOT NULL,
    email char(50) not null unique,
    telephone integer not null,
    date_of_birth datetime not null,
    address text not null,
    password char(50) not null
);

-- Table for accounts
CREATE TABLE Account (
    account_id INTEGER PRIMARY KEY,
    client_id INTEGER,
    balance REAL NOT NULL,
    currency TEXT,
    FOREIGN KEY (client_id) REFERENCES Client(client_id)
);

-- Table for operations
CREATE TABLE Operation (
    operation_id INTEGER PRIMARY KEY,
    account_id INTEGER,
    amount REAL NOT NULL,
    operation_type TEXT NOT NULL,
    operation_date DATE,
    FOREIGN KEY (account_id) REFERENCES Account(account_id)
);
