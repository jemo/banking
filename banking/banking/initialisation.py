#!/usr/bin/python
# -*-coding:Utf-8 -*
# jemo 8 juin 2015 mis a jour pour python 3
# jemo 11 juin 2020 mise en forme projet

"""
    Module d'initialisation du system
"""

import logging
import os
import sqlite3

# Gestion des log
logging.basicConfig(filename="log", level=logging.DEBUG, \
                    format="%(asctime)s %(message)s", datefmt="%d/%m/%Y %H:%M:%S")


def init_system():
    """
    Fonction pour initialiser le systeme ou vérifier que
    les bases sont présentent
    """
    print("Init system")
    logging.info(" >> debut init_system() --> STATUS: SUCCESS")
    db_filename = "./banking/data/banking.db"
    print(check_database(db_filename))
    if check_database(db_filename):
        pass
    else:
        create_database(db_filename)
    # os.system("clear")


def check_database(db_filename):
    """
    Fonction pour tester la présence de la base de donnée
    """
    return os.path.exists(db_filename)


def create_database(db_filename):
    """
    Fonction pour créer la base de donnée
    """
    schema_filename = "./banking/banking/banking_schema.sql"
    print(db_filename)
    with sqlite3.connect(db_filename) as conn:
        print('Creating schema')
        with open(schema_filename, 'rt') as file_open:
            schema = file_open.read()
            conn.executescript(schema)


if __name__ == '__main__':
    init_system()
