#!/usr/bin/python
# -*-coding:Utf-8 -*
# jemo 8 juin 2015 mis a jour pour python 3
# jemo 11 juin 2020 mise en forme projet

"""
    Module qui doit etre l'interface avec les autre modules du back
"""

import os
import csv
import logging
from time import sleep

from translation import select_language
from initialisation import init_system

# Gestion des log
logging.basicConfig(filename='log', level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %H:%M:%S')


def front_cli():
    """
    Fonction principale de l'interface CLI
    """
    logging.info(" >> debut front_cli() --> STATUS: SUCCESS")
    language = welcome()
    lang = select_language(language)
    init_system()
    check_new_client(lang)
    check_register(lang)


def check_new_client(lang):
    """
    Fonction qui demande si le client est nouveau
    """
    logging.info(" >> debut check_new_client() --> STATUS: SUCCESS")
    new_user = ""
    while new_user not in ["Y", "y", "N", "n"]:
        new_user = input(lang[1])
    quitter(lang)


def check_register(lang):
    """
    Fonction qui vérifie si l'utilisateur existe
    """
    logging.info(" >> debut check_register() --> STATUS: SUCCESS")
    question = ''
    while question != 'Q':
        question = input(lang[1])
        if question == 'Y' or question == 'y':
            if not os.path.exists('users.txt'):
                write_to_file_add('users', '\n', 'new')
                account, lang = create_pin(lang)
            else:
                account = check_account(lang)
                check_pin(account, lang)
            choice(account, lang)
        elif question == 'N' or question == 'n':
            account, lang = create_pin(lang)
            choice(account, lang)
        elif question == 'X' or question == 'x':
            admin(lang)
        elif question == 'Q' or question == 'q':
            quitter(lang)
        else:
            print(lang[32])


def check_account(lang):
    """
    Fonction pour verifier si l'utilisateur a un compte
    """
    account_number_input = input(lang[27])
    account_number_input_confirm = input(lang[28])
    while (account_number_input != account_number_input_confirm) or \
            (account_number_input == account_number_input_confirm == ''):
        print(lang[4])
        account_number_input = input(lang[27])
        account_number_input_confirm = input(lang[28])
    # print account_number_input
    with open('users.txt', 'r') as searchfile:
        for line in searchfile:
            if account_number_input in line:
                # print(line.strip('\n')
                line = line.strip('\n')
                searchfile.close()
                break
            else:
                pass
            # print 'Did not find ...' + str(account_number_input)
    if account_number_input == account_number_input_confirm:
        if line == account_number_input:
            account = account_number_input_confirm
            return account
        elif line != account_number_input:
            account = account_number_input_confirm
            logging.info(" >> %s >> new account --> STATUS: SUCCESS", account)
            print(lang[29])
        else:
            print(lang[30])
    else:
        print(lang[30])


def check_pin(account, lang):
    """
    Fonction de vérification de l'acces au compte
    TODO : utilisation de getpass
    """
    pin_input = input(lang[5])
    pin_file = read_file(account)
    if pin_input != pin_file:
        while pin_input != pin_file:
            print(lang[7])
            pin_input = input(lang[5])
    else:
        print(lang[31])
        logging.info(" >> %e >> Password Accepted --> STATUS: SUCCESS", account)


def create_pin(lang):
    """
    Fonction de creation du mot de passe
    TODO : utilisation de getpass
    """
    account_number_input = input(lang[2])
    account_number_input_confirm = input(lang[3])
    while (account_number_input != account_number_input_confirm) or \
            (account_number_input == account_number_input_confirm == ''):
        print(lang[4])
        account_number_input = input(lang[2])
        account_number_input_confirm = input(lang[3])
    account = account_number_input_confirm
    try:
        # question = input('Do You Want To Create An Account ? (Yes \'Y\' or No \'N\')')
        # if question == 'Y':
        pin_input = input(lang[5])
        pin_input_confirm = input(lang[6])
        while pin_input != pin_input_confirm:
            print(lang[7])
            pin_input = input(lang[5])
            pin_input_confirm = input(lang[6])
        pin = pin_input_confirm
        # print 'pin Correct'
        write_to_file_add('users', '\n' + account, account)
        write_to_file_erase(account, pin, account)
        write_to_file_erase(account + '_balance', '0', account)
        print(lang[8])
        logging.info(" >> %e >> create_pin() --> STATUS: SUCCESS", account)
        collect_infos(account, lang)
        return account, lang
    # else:
    #   banking()
    except:
        print(lang[9])


def collect_infos(account, lang):
    """
    Fonction de collecte des informations du client
        est appelé à l'ouverture d'un nouveau compte
    """
    print(lang[10])
    name = input(lang[11])
    first_name = input(lang[12])
    birthdate = input(lang[13])
    address = input(lang[14])
    data = name + '\n' + first_name + '\n' + birthdate + '\n' + address + '\n'
    name = account + '_infos'
    write_to_file_add(name, data, account)
    logging.info(" >> %e >> collect_infos() --> STATUS: SUCCESS", account)


def write_to_file_add(name, data, account):
    try:
        fob = open(name + '.txt', 'a')
        fob.write(data)
        fob.close()
        logging.info(' >> ' + str(account) + ' >> write_to_file_add() --> STATUS: SUCCESS')
    except:
        print('Error write add')


def write_to_file_erase(name, data, account):
    try:
        fob = open(name + '.txt', 'w')
        fob.write(data)
        fob.close()
        logging.info(' >> ' + str(account) + ' >> write_to_file_erase() --> STATUS: SUCCESS')
    except:
        print('Error write erase')


def write_csv(name, data, account):
    try:
        fob = open(name + '.csv', 'a')
        fob.write(data)
        fob.close()
        logging.info(' >> ' + str(account) + ' >> write_to_file_erase() --> STATUS: SUCCESS')
    except:
        logging.info(' >> ' + str(account) + ' >> write_csv() --> STATUS: FAIL')
        print('Error write')


def read_file(name):
    try:
        fob = open(name + '.txt', 'r')
        file_line = fob.read()
        fob.close()
        return file_line
    except:
        print('Error read')


def choice(account, lang):
    """
    Fonction principale de gestion et consultation du compte client
    """
    logging.info(' >> ' + str(account) + ' >> debut choice() --> STATUS: SUCCESS')
    print(lang[15])
    print(lang[16], account, ' ?')
    choice = ''
    while choice != 'Q':
        sleep(1)
        choice = input('\n' + lang[17] + '\n' + lang[18])
        if choice == 'A' or choice == 'a':
            check_balance(account, lang)
        elif choice == 'B' or choice == 'b':
            add_amount(account, lang)
        elif choice == 'C' or choice == 'c':
            print_ops(account, lang)
        # elif choice == 'E' or choice == 'e':
        # print('test')
        # send_data(account, lang)
        elif choice == 'M' or choice == 'm':
            check_register(lang)
        elif choice == 'Q' or choice == 'q':
            quitter(lang)
        else:
            print(lang[32])


def check_balance(account, lang):
    """
    Fonction de récupération du solde du compte
    """
    logging.info(' >> ' + str(account) + ' >> debut check_balance() --> STATUS: SUCCESS')
    balance = read_file(account + '_balance')
    print(lang[19], balance, 'Euros')
    if float(balance) < 0:
        print('\n*****************\n***' + lang[20] + '***\n*****************\n')
    return balance, lang


def add_amount(account, lang):
    """
    Fonction pour ajouter opération au compte
    """
    logging.info(' >> ' + str(account) + ' >> debut add_amount() --> STATUS: SUCCESS')
    balance, lang = check_balance(account, lang)
    amount = 0.0
    while amount is not int:
        try:
            if not os.path.exists(account + '_ops.csv'):
                write_csv(account + '_ops',
                          'Date de l''operation;Date de valeur;Libelle de l''operation;Debit;Credit;\n', account)
            # else:
            #   continue
            amount = input(lang[21])
            libel = input(lang[22])
            dateop = input(lang[23])
            new_balance = float(balance) + float(amount)
            write_to_file_erase(account + '_balance', str(new_balance), account)
            if int(amount) < 0:
                data = dateop + ';' + dateop + ';' + libel + ';' + str(amount) + ';;'
            elif int(amount) > 0:
                data = dateop + ';' + dateop + ';' + libel + ';;' + str(amount) + ';'
            # print(data)
            write_csv(account + '_ops', data + '\n', account)
            check_balance(account, lang)
            break
        except:
            print(lang[24])
            break


def print_ops(account, lang):
    """
    Fonction de restitution des opération d'un compte
    """
    logging.info(' >> ' + str(account) + ' >> debut print_ops() --> STATUS: SUCCESS')
    try:
        fob = open(account + '_ops.csv')
        fob1 = csv.reader(fob, delimiter=';')
        rownum = 0
        for row in fob1:
            if rownum == 0:
                header = row
            else:
                column = 0
                for col in row:
                    print(header[column], ' : ', col)
                    column += 1
            rownum += 1
        fob.close()
    except:
        print(lang[25])


def admin(lang):
    logging.info('  >> debut admin() --> STATUS: SUCCESS')
    pin_admin = input(lang[35])
    if pin_admin == '1103':
        choice_admin = input(lang[36])
        if choice_admin == 'E' or choice_admin == 'e':
            epuration = input(lang[37])
            if epuration == 'Y' or epuration == 'y':
                print(lang[38])
                epur(lang)
            else:
                pass
        elif choice_admin == 'M' or choice_admin == 'm':
            check_register(lang)
        elif choice_admin == 'Q' or choice_admin == 'q':
            quitter(lang)
        else:
            print(lang[32])
    else:
        print(lang[39])
        logging.info('  >> admin failed --> STATUS: SUCCESS')


def epur(lang):
    print(lang[40])
    logging.info(' >> debut epur() --> STATUS: SUCCESS')
    with open('users.txt', 'r') as searchfile:
        for account in searchfile:
            account = account.strip('\n')
            try:
                print(lang[41], account)
                os.remove(account + '.txt')
                os.remove(account + '_infos.txt')
                os.remove(account + '_balance.txt')
                os.remove(account + '_ops.csv')
            except:
                print(lang[42])
    searchfile.close()
    os.remove('users.txt')
    quitter(lang)


def welcome():
    """
    Fonction d'acceuil
    Pour déterminer la langue de l'interface
    """
    logging.info(' >> debut welcome() --> STATUS: SUCCESS')
    language = input('Please select your language : Francais \'F\' or English \'E\' : ')
    while (language != 'F') or (language != 'f') or (language != 'E') or (language != 'e'):
        if language == 'F' or language == 'f':
            language = "french"
            break
        elif language == 'E' or language == 'e':
            language = "english"
            break
        else:
            print('Invalide choice')
            language = input('Please select your language : Francais \'F\' or English \'E\' : ')
    return language


def quitter(lang):
    print(lang[26])
    sleep(1)
    exit()


if __name__ == '__main__':
    front_cli()
