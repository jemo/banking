#!/usr/bin/python
# -*-coding:Utf-8 -*
# Import smtplib for the actual sending function
import smtplib
from email.mime.text import MIMEText
import datetime

#Import sys to deal with command line arguments
import sys
import os
import csv
#import send_mail
from time import strftime
logfile = open('log.txt', 'w')
heure = strftime("%H:%M:%S")
import time

#creation des liste de langues
french = []
english = []
filin = 'french.csv'
fopen = open(filin, 'rb')
reader = csv.reader(fopen)
for row in reader:
        french = french + row
fopen.close()
filin = 'english.csv'
fopen = open(filin, 'rb')
reader = csv.reader(fopen)
for row in reader:
        english = english + row
fopen.close()
#print french[0]
#print english[0]


def main():
    logfile.write(str(heure)+ '  >> dÃ©but main() --> STATUS: SUCCESS \n')
    language = raw_input('Please select your language : Francais \'F\' or English \'E\' : ')
    if language == 'F' or language == 'f':
        lang = french
        #print lang[0]
    elif language == 'E' or language == 'e':
        lang = english
        #print lang[0]
    else:
        print 'Invalide choice'
    print lang[0]
    if not os.path.exists('users.txt'):
        write_to_file_add('users','\n','new')
    check_register(lang)
    logfile.close()
    
def check_account(lang):
    account_number_input = raw_input(lang[27])
    account_number_input_confirm = raw_input(lang[28])
    while (account_number_input != account_number_input_confirm) or (account_number_input == account_number_input_confirm == ''):
        print lang[4]
        account_number_input = raw_input(lang[27])
        account_number_input_confirm = raw_input(lang[28])
    #print account_number_input
    with open('users.txt', 'r') as searchfile:
        for line in searchfile:
            if account_number_input in line:
                #print line.strip('\n')
                line = line.strip('\n')
                searchfile.close()
                break
            else:
                pass
                #print 'Did not find ...' + str(account_number_input)
    if account_number_input == account_number_input_confirm:
        if line == account_number_input:
            account = account_number_input_confirm
            return account
        elif line != account_number_input:
            account = account_number_input_confirm
            logfile.write(str(heure)+ ' >> ' + str(account) + ' >> new account --> STATUS: SUCCESS \n')
            print lang[29]
            check_register(lang)
        else:
            print lang[30]
    else:
        print lang[30]

def check_pin(account,lang):
    PIN_input = raw_input(lang[5])
    PIN_file = read_file(account)
    if PIN_input != PIN_file:
        while PIN_input != PIN_file:
            print lang[7]
            PIN_input = raw_input(lang[5])
    else:
        print lang[31]
        logfile.write(str(heure)+ ' >> ' + str(account) +' >> Password Accepted --> STATUS: SUCCESS \n')

def create_pin(lang):
    account_number_input = raw_input(lang[2])
    account_number_input_confirm = raw_input(lang[3])
    while (account_number_input != account_number_input_confirm) or (account_number_input == account_number_input_confirm == ''):
        print lang[4]
        account_number_input = raw_input(lang[2])
        account_number_input_confirm = raw_input(lang[3])
    account = account_number_input_confirm
    try:
        #question = raw_input('Do You Want To Create An Account ? (Yes \'Y\' or No \'N\')')
        #if question == 'Y':
        PIN_input = raw_input(lang[5])
        PIN_input_confirm = raw_input(lang[6])
        while PIN_input != PIN_input_confirm:
            print lang[7]
            PIN_input = raw_input(lang[5])
            PIN_input_confirm = raw_input(lang[6])
        PIN = PIN_input_confirm
           # print 'PIN Correct'
        write_to_file_add('users','\n'+account,account)
        write_to_file_erase(account,PIN,account)
        write_to_file_erase(account+'_balance','0',account)
        print lang[8]
        logfile.write(str(heure)+ ' >> ' + str(account) + ' >> create_pin() --> STATUS: SUCCESS \n')
        collect_infos(account,lang)
        return account, lang
        #else:
         #   banking()
    except:
        print lang[9]
        
def collect_infos(account,lang):
    print lang[10]
    name      = raw_input(lang[11])
    first_name   = raw_input(lang[12])
    birthdate = raw_input(lang[13])
    address    = raw_input(lang[14])
    data = name+'\n'+first_name+'\n'+birthdate+'\n'+address+'\n'
    name = account+'_infos'
    write_to_file_add(name, data,account)
    logfile.write(str(heure)+ ' >> ' + str(account) + ' >> collect_infos() --> STATUS: SUCCESS \n')

def write_to_file_add(name, data,account):
    try:
        fob = open(name+'.txt','a')
        fob.write(data)
        fob.close()
        logfile.write(str(heure)+ ' >> ' + str(account) + ' >> write_to_file_add() --> STATUS: SUCCESS \n')
    except:
        print "Error write add"

def write_to_file_erase(name, data,account):
    try:
        fob = open(name+'.txt','w')
        fob.write(data)
        fob.close()
        logfile.write(str(heure)+ ' >> ' + str(account) + ' >> write_to_file_erase() --> STATUS: SUCCESS \n')
    except:
        print "Error write erase"

def write_csv(name, data, account):
    try:
        fob = open(name+'.csv','a')
        fob.write(data)
        fob.close()
        logfile.write(str(heure)+ ' >> ' + str(account) + ' >> write_to_file_erase() --> STATUS: SUCCESS \n')
    except:
        logfile.write(str(heure)+ ' >> ' + str(account) + ' >> write_csv() --> STATUS: FAIL \n')
        print "Error write"
        
def read_file(name):
    try:
        fob = open(name+'.txt','r')
        read_file = fob.read()
        return read_file
        fob.close()
    except:
        print 'Error read'

def choice(account,lang):
    logfile.write(str(heure)+ ' >> ' + str(account) + ' >> dÃ©but choice() --> STATUS: SUCCESS \n')
    print lang[15]
    print lang[16], account, ' ?'
    choice = ''
    while choice != 'Q':
        time.sleep(1.0)
        choice = raw_input('\n'+lang[17]+'\n'+lang[18])
        if choice == 'A' or choice == 'a':
            check_balance(account,lang)
        elif choice == 'B' or choice == 'b':
            add_amount(account,lang)
        elif choice == 'C' or choice == 'c':
            print_ops(account,lang)
        elif choice == 'E' or choice == 'e':
            print 'test'
            send_data(account,lang)
        elif choice == 'M' or choice == 'm':
            check_register(lang)
        elif choice == 'Q' or choice == 'q':
            quitter(lang)
        else:
            print lang[32]

def check_balance(account,lang):
    logfile.write(str(heure)+ ' >> ' + str(account) + ' >> dÃ©but check_balance() --> STATUS: SUCCESS \n')
    balance = read_file(account+'_balance')
    print lang[19], balance, 'Euros'
    if float(balance) < 0:
        print '\n********************************\n***'+lang[20]+'***\n********************************\n'
    return balance, lang

def add_amount(account,lang):
    logfile.write(str(heure)+ ' >> ' + str(account) + ' >> dÃ©but add_amount() --> STATUS: SUCCESS \n')
    balance,lang = check_balance(account,lang)
    amount = 0.0
    while amount is not float:
        try:
            if not os.path.exists(account+'_ops.csv'):
                write_csv(account+'_ops','Date de l''operation;Date de valeur;Libelle de l''operation;Debit;Credit;\n',account)
            #else:
             #   continue
            amount = input(lang[21])
            libel = ''
            libel = raw_input(lang[22])
            dateop = ''
            dateop = raw_input(lang[23])
            new_balance = float(balance) + float(amount)
            write_to_file_erase(account+'_balance',str(new_balance),account)
            if amount < 0:
                data = dateop+';'+dateop+';'+libel+';'+str(amount)+';;'
            elif amount > 0:
                data = dateop+';'+dateop+';'+libel+';;'+str(amount)+';'
            #print data
            write_csv(account+'_ops', data+'\n',account)
            check_balance(account,lang)
            break
        except:
            print lang[24]
            break

def send_data(account,lang):
    try:
        DESTINATAIRE = raw_input(lang[33])
        # A COLLER DANS LE PROGRAMME
        #Informations fonctionnelles, à changer en fonctions
        print 'DESTINATAIRE : ', DESTINATAIRE
        balance = read_file(account+'_balance')
        SUJET = 'Informations sur votre compte '+ account
        print 'SUJET : ', SUJET
        MESSAGE = 'Votre compte est crediteur de '+ balance+ ' euros'
        print 'MESSAGE : ', MESSAGE
        #Fonctions
        #send_mail(DESTINATAIRE, SUJET, MESSAGE)
    except:
        print lang[34]
        
def send_mail(DESTINATAIRE, SUJET, MESSAGE):
    # A COLLER AU DEBUT DU PROGRAMME
    # Informations techniques, à ne pas changer
    gmail_user = 'xx@gmail.com'
    gmail_password = '**'
    smtpserver = smtplib.SMTP('smtp.gmail.com', 587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(gmail_user, gmail_password)
    today = datetime.date.today()
    msg = MIMEText(MESSAGE)
    msg['Subject'] = SUJET
    msg['From'] = gmail_user
    msg['To'] = DESTINATAIRE
    smtpserver.sendmail(gmail_user, [DESTINATAIRE], msg.as_string())
    smtpserver.quit()

def print_ops(account,lang):
    logfile.write(str(heure)+ ' >> ' + str(account) + ' >> dÃ©but print_ops() --> STATUS: SUCCESS \n')
    try:
        fob = open(account+'_ops.csv','rb')
        fob1 = csv.reader(fob, delimiter=';')
        rownum = 0
        for row in fob1:
            if rownum == 0:
                header = row
            else:
                colnum = 0
                for col in row:
                    print header[colnum], ' : ', col
                    colnum += 1
            rownum += 1
        fob.close()
    except:
        print lang[25]

def check_register(lang):
    logfile.write(str(heure)+ '  >> dÃ©but check_register() --> STATUS: SUCCESS \n')
    question = ''
    while question != 'Q':
        question = raw_input(lang[1])
        if question == 'Y' or question == 'y':
            account = check_account(lang)
            check_pin(account,lang)
            choice(account,lang)
        elif question == 'N' or question == 'n':
            account,lang = create_pin(lang)
            choice(account,lang)
        elif question == 'X' or question == 'x':
            admin(lang)
        elif question == 'Q' or question == 'q':
            quitter(lang)
        else:
            print lang[32]
            
def admin(lang):
    logfile.write(str(heure)+ '  >> dÃ©but admin() --> STATUS: SUCCESS \n')
    PIN_admin = raw_input(lang[35])
    if PIN_admin == '1103':
        choice_admin = raw_input(lang[36])
        if choice_admin == 'E' or choice_admin == 'e':
            epuration = raw_input(lang[37])
            if epuration == 'Y' or epuration == 'y':
                print lang[38]
                epur(lang)
            else:
                pass
        elif choice_admin == 'M' or choice_admin == 'm':
            check_register(lang)
        elif choice_admin == 'Q' or choice_admin == 'q':
            quitter(lang)
        else:
            print lang[32]
    else:
        print lang[39]
        logfile.write(str(heure)+ ' >> admin failed --> STATUS: SUCCESS \n')

def epur(lang):
    print lang[40]
    logfile.write(str(heure)+ ' >> debut epur() --> STATUS: SUCCESS \n')
    with open('users.txt', 'r') as searchfile:
        for account in searchfile:
            account = account.strip('\n')
            try:
                print lang[41], account
                os.remove(account+'.txt')
                os.remove(account+'_infos.txt')
                os.remove(account+'_balance.txt')
                os.remove(account+'_ops.csv')
            except:
                print lang[42]
    searchfile.close()
    os.remove('users.txt')
    quitter(lang)

def quitter(lang):
    print lang[26]
    logfile.close()
    time.sleep(1)
    exit()

if __name__ == '__main__':
    main()
